<?PHP
/**
 * Plugin Name: WCP (WooCommerce Customizer plugin)
 * Description: Woocommerce Customizer Plugin
 * Plugin URI:  https://iranimij.com
 * Version:     1.0.0
 * Author:      Iman heydari
 * Author URI:  https://iranimij.com
 * Text Domain: ela-extension
 */

// after loaded plugin
add_action("plugins_loaded", "init_wcp");

function init_wcp()
{

    // move sku and category
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
    add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5);

    // related product limitation
    add_filter('woocommerce_output_related_products_args', 'change_number_related_products', 9999);

    // woocommerce_locate_template
    add_filter('woocommerce_locate_template', 'myplugin_woocommerce_locate_template', 10, 3);

}

function change_number_related_products($args)
{
    $args['posts_per_page'] = 3;
    $args['columns'] = 3;
    return $args;
}

// gets the absolute path to this plugin directory

function myplugin_plugin_path()
{
    return untrailingslashit(plugin_dir_path(__FILE__));
}


function myplugin_woocommerce_locate_template($template, $template_name, $template_path)
{
    global $woocommerce;

    $_template = $template;

    if (!$template_path) $template_path = $woocommerce->template_url;

    $plugin_path = myplugin_plugin_path() . '/woocommerce/';

    // Look within passed path within the theme - this is priority
    $template = locate_template(

        array(
            $template_path . $template_name,
            $template_name
        )
    );

    // Modification: Get the template from this plugin, if it exists
    if (!$template && file_exists($plugin_path . $template_name))
        $template = $plugin_path . $template_name;

    // Use default template
    if (!$template)
        $template = $_template;

    // Return what we found
    return $template;
}